const mongoose = require('mongoose');

const { RAB_MONGO_DB_ADDRESS, RAB_MONGO_DB_NAME } = process.env;

module.exports.createStore = () => {
    mongoose.connect(`${encodeURI(RAB_MONGO_DB_ADDRESS)}/${RAB_MONGO_DB_NAME}`, {
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
    });
    mongoose.connection.on('error', error => console.log(error) );
    mongoose.Promise = global.Promise;

    const calendarSchema = new mongoose.Schema({
        userServiceId: String,
        plants: [{ type: String, ref: 'Plants' }],
    });

    const Calendar = mongoose.model('Calendar', calendarSchema);

    const plantSchema = new mongoose.Schema({
        _id: String,
        name: String,
        species: [{ type: String, ref: 'Species' }],
    });

    const Plants = mongoose.model('Plants', plantSchema);

    const speciesSchema = new mongoose.Schema({
        _id: String,
        name: String,
        plants: [{ type: String, ref: 'Plants' }],
    });

    const Species = mongoose.model('Species', speciesSchema);

    return { Calendar, Plants, Species };
};
