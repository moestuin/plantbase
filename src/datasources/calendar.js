const {DataSource} = require('apollo-datasource');
const {ApolloError} = require('apollo-server');

class CalendarApi extends DataSource {
    constructor({store}) {
        super();
        this.store = store;
    }

    userCheck() {
        if (!this.context.user || !this.context.user._id) {
            throw new ApolloError('User needs to login')
        }
    }

    /**
     * This is a function that gets called by ApolloServer when being setup.
     * This function gets called with the datasource config including things
     * like caches and context. We'll assign this.context to the request context
     * here, so we can know about the user making requests
     */
    initialize(config) {
        this.context = config.context;
    }

    async getPlants() {
        this.userCheck();

        const {_id} = this.context.user;
        const {Calendar} = this.store;

        return await Calendar.findOne({_id}, {plants: 1}).populate('plants');
    }

    async addPlants({plantIds}) {
        this.userCheck();

        const {_id} = this.context.user;
        const {Calendar} = this.store;

        // TODO: Check if plant exists before adding

        const {plants} = await Calendar.findOneAndUpdate(
            {
                _id
            },
            {
                _id,
                $addToSet: {
                    plants: {$each: plantIds}
                }
            },
            {
                fields: {plants: 1},
                upsert: true,
                new: true
            }
        ).populate({
            path: 'plants',
            match: {_id: {$in: plantIds}}
        });

        return {
            success: plants.length === plantIds.length,
            failed: plantIds.filter(id => !plants.map(({_id}) => _id).includes(id)),
            plants
        }
    }

    async removePlants({plantIds}) {
        this.userCheck();

        const {_id} = this.context.user;
        const {Calendar} = this.store;

        // TODO: Check if plant exists before removing

        const {plants} = await Calendar.findOneAndUpdate(
            {
                _id
            },
            {
                $pull: {
                    plants: {$in: plantIds}
                }
            },
            {
                fields: {plants: 1},
                new: true
            }
        );

        const failedRemovedPlants = plants.filter(id => plantIds.includes(id));

        return {
            success: failedRemovedPlants.length === 0,
            failed: failedRemovedPlants.map(({_id}) => _id),
            plants: [],
        }
    }
}

module.exports = CalendarApi;
