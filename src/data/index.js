const plants = [{
    id: '1',
    name: 'Tatsoi',
    species: 'Brassica rapa',
}, {
    id: '2',
    name: 'Snijbiet',
    species: 'Beta vulgaris'
}, {
    id: '3',
    name: 'Field mustard',
    species: 'Brassica rapa'
}];

const species = [{
    id: '1',
    name: 'Brassica rapa',
}, {
    id: '2',
    name: 'Beta vulgaris'
}];

const getPlants = () => new Promise((resolve) => resolve(plants));
const getSpecies = () => new Promise((resolve) => resolve(species));

exports.getPlants = getPlants;
exports.getSpecies = getSpecies;