const { gql } = require('apollo-server');

const typeDefs = gql`
    type Specie {
        id: ID,
        name: String,
    }

    type Plant {
        id: ID,
        name: String,
        species: [Specie],
    }

    type Species {
        id: ID,
        name: String,
        plants: [Plant],
    }

    type Calendar {
        id: ID,
        plants: [Plant],
    }
    
    type Query {
        species: [Species],
        plants: [Plant],
        calendar: Calendar,
        plant(plantId: String!): Plant,
    }

    type Mutation {
        addPlants(plantIds: [ID]!): PlantSaveResponse!
        removePlants(plantIds: [ID]!): PlantRemovedResponse!
    }

    type PlantSaveResponse {
        success: Boolean!
        failed: [String]
        plants: [Plant]
    }

    type PlantRemovedResponse {
        success: Boolean!
        failed: [String]
        plants: [Plant]
    }
`;

module.exports = typeDefs;
