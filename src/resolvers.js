const {ApolloError, ValidationError} = require('apollo-server');

module.exports = {
    Query: {
        species: async (_, __, {dataSources: {plantsApi}}) => {
            try {
                return await plantsApi.getSpecies();
            } catch (error) {
                throw new ApolloError(error);
            }
        },
        plants: async (_, __, {dataSources: {plantsApi}}) => {
            try {
                return await plantsApi.getPlants();
            } catch (error) {
                throw new ApolloError(error);
            }
        },
        plant: async (_, {plantId}, {dataSources: {plantsApi}}) => {
            try {
                const plant = await plantsApi.getPlantById({plantId});
                return plant ? plant : new ValidationError(`Plant with id ${plantId} not found`)
            } catch (error) {
                throw new ApolloError(error);
            }
        },
        calendar: async (_, __, {dataSources: {calendarApi}}) => {
            try {
                return await calendarApi.getPlants();
            } catch (error) {
                throw new ApolloError(error);
            }
        },
    },
    Mutation: {
        addPlants: async (_, {plantIds}, {dataSources: {calendarApi}}) => calendarApi.addPlants({plantIds}),
        removePlants: async (_, {plantIds}, {dataSources: {calendarApi}}) => calendarApi.removePlants({plantIds})
    }
};
