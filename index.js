'use strict';

const express = require('express');
const graphqlHTTP = require('express-graphql');
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLNonNull,
    GraphQLList
} = require('graphql');

const PORT = process.env.port || 3000;
const server = express();

const friendType = new GraphQLObjectType({
    name: 'FriendType',
    fields: {
        id: {
            type: GraphQLID,
            description: 'Plant ID'
        },
        name: {
            type: GraphQLString,
            description: 'General name of the plant'
        },
        species: {
            type: GraphQLString,
            description: 'The species this plant belongs to'
        }
    }
});

const plantType = new GraphQLObjectType({
    name: 'PlantType',
    fields: {
        id: {
            type: GraphQLID,
            description: 'Plant ID'
        },
        name: {
            type: GraphQLString,
            description: 'General name of the plant'
        },
        species: {
            type: GraphQLString,
            description: 'The species this plant belongs to'
        },
        friends: {
            type: GraphQLList(friendType),
            description: 'Friends which are in the same species'
        }
    }
});

const speciesType = new GraphQLObjectType({
    name: 'SpeciesType',
    fields: {
        id: {
            type: GraphQLID,
            description: 'Plant ID'
        },
        name: {
            type: GraphQLString,
            description: 'General name of the plant'
        },
        plants: {
            type: GraphQLList(plantType),
            description: 'Plants which in this species'
        }
    }
});

const queryType = new GraphQLObjectType({
    name: 'QueryType',
    description: 'The Root query type',
    fields: {
        species: {
            type: new GraphQLList(speciesType),
            resolve: async () => {
                const species = await getSpecies();

                return species.map(async (species) => {
                    return ({
                        ...species,
                        plants: await getPlantBySpecies(species.name)
                    });
                })
            }
        },
        plants: {
            type: new GraphQLList(plantType),
            resolve: async () => {
                const plants = await getPlants();

                return plants.map(async (plant) => {
                    return ({
                        ...plant,
                        friends: (await getPlantBySpecies(plant.species)).filter(({id}) => id !== plant.id)
                    });
                })
            }
        },
        plant: {
            type: plantType,
            args: {
                id: {
                    type: new GraphQLNonNull(GraphQLID),
                    description: 'The id of the plant'
                }
            },
            resolve: async (_, args) => {
                const plant = await getPlantById(args.id);
                return {
                    ...plant,
                    friends: (await getPlantBySpecies(plant.species)).filter(({id}) => id !== plant.id)
                };
            }
        }
    }
});

const schema = new GraphQLSchema({
    query: queryType,
});

server.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

server.listen(PORT, () => {
    console.log(`Listening on localhost:3000`)
});