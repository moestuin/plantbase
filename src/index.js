const {ApolloServer} = require('apollo-server-express');
const express = require('express');
const passport = require('passport');

const typeDefs = require('./schema');
const resolvers = require('./resolvers');

const CalendarApi = require('./datasources/calendar');
const PlantsApi = require('./datasources/plants');
const {createStore} = require('./utils/store');

const {GRAPHQL_PORT = 4000} = process.env;

const app = express();

require('./auth/auth');

app.use('/graphql', (req, res, next) => {
    passport.authenticate('jwt', {session: false}, (err, user) => {
        req.user = user;

        next()
    })(req, res, next)
});

const context = async ({req}) => ({
    user: req.user
});

const store = createStore();

const dataSources = () => {
    return ({
        plantsApi: new PlantsApi({store}),
        calendarApi: new CalendarApi({store}),
    });
};

const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources,
    context,
    engine: {
        apiKey: "service:studiorabota-8013:zILDSxqYu3sW6CiHOg77cQ"
    },
});

server.applyMiddleware({
    app
});

app.listen(
    {
        port: GRAPHQL_PORT
    },
    () => console.log(`🚀 Server ready at localhost:${GRAPHQL_PORT}/graphql`)
);