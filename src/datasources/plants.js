const {DataSource} = require('apollo-datasource');

// TODO: Pre-load all plants on start server

class SearchApi extends DataSource {
    constructor({store}) {
        super();
        this.store = store;
    }

    async getPlants() {
        const {Plants} = this.store;

        return Plants.find({}).populate('species');
    }

    async getSpecies() {
        const {Species} = this.store;

        return Species.find({}).populate('plants');
    }

    async getPlantById({plantId}) {
        const {Plants} = this.store;

        return Plants.findById(plantId);
    }
}

module.exports = SearchApi;
